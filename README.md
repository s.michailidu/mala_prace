# Malá práce

Použití generátoru statických stránek a kontinuální integrace.

# Použití
Vygenerovaná stránka je na adrese: https://s.michailidu.gitlab.io/mala_prace/

# Prerekvizity
`python`, `virtualenv`

# Run
1. Vytvoření a aktivace virtuálního prostředí
```
virtualenv venv
source venv/bin/activate
```

2. Instalace requirements.txt
``` 
pip install -r requirements.txt
```

3. Spuštění serveru
```
mkdocs serve
```


